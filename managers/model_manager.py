import os
import numpy as np
import tensorflow as tf
from helper import global_variables as gv
from sklearn.model_selection import train_test_split
from helper.utils import (prepare_directory, write_json_file, read_json_file)
from typing import Optional
from keras import optimizers
from helper.sequence_generator import DataGenerator
from keras.models import Model, load_model
from keras.layers import (Input, Dense)
from managers.mongodb_manager import MongoDBManager
from keras.callbacks import History, EarlyStopping


class ModelManager:
    def __init__(self, input_dim: int, output_dim: int):
        self.test_size = gv.test_size
        self.dev_size = gv.dev_size
        self.input_dim: int = input_dim
        self.output_dim: int = output_dim
        self.model_name: str = gv.model_name
        self.metrics: list = gv.metrics
        self.losses: str = gv.loss
        self.optimizer_name: str = gv.optimizer_name
        self.batch_size: int = gv.batch_size
        self.epochs: int = gv.epochs
        self.callbacks: list = [EarlyStopping(monitor=gv.monitor_early_stopping,
                                              patience=gv.patience)]

    def generate_train_dev_test_sets(self, all_docs_ids: list):
        train_ids: list = []
        dev_ids: list = []
        test_ids: list = []
        try:
            # Divide document ids into train and test
            train_ids, test_ids = train_test_split(all_docs_ids,
                                                   test_size=self.test_size)

            train_ids, dev_ids = train_test_split(train_ids,
                                                  test_size=self.dev_size)
        except Exception as e:
            gv.logger.error(e)
        return train_ids, dev_ids, test_ids

    @staticmethod
    def build_bot_net(input_dim: int,
                      output_dim: int,
                      intermediate_neurons: int,
                      intermediate_layer: str,
                      model_name: str, output_layer: str):
        bot_net: Optional[Model] = None
        try:
            # Create inputs
            input_data: Input = Input((input_dim,))
            x: Dense = Dense(intermediate_neurons, activation=intermediate_layer)(input_data)
            x: Dense = Dense(int(np.ceil(intermediate_neurons/2)), activation=intermediate_layer)(x)
            output_data: Dense = Dense(output_dim, activation=output_layer)(x)

            bot_net: Model = Model(
                inputs=input_data,
                outputs=output_data,
                name=model_name)
            # Show summary
            bot_net.summary()
        except Exception as e:
            gv.logger.error(e)
        return bot_net

    @staticmethod
    def select_optimizer(opt_name="adam"):
        optimizer: Optional[optimizers] = None
        try:
            if opt_name == 'sgd':
                optimizer = optimizers.SGD()
            elif opt_name == 'adagrad':
                optimizer = optimizers.Adagrad()
            elif opt_name == 'adadelta':
                optimizer = optimizers.Adagrad()
            elif opt_name == 'adam':
                optimizer = optimizers.Adam()
            elif opt_name == 'nadam':
                optimizer = optimizers.Nadam()
            else:
                gv.logger.warning('Not valid optimizer!. Please check the configuration script.')
        except Exception as e:
            gv.logger.error(e)
        return optimizer

    def compile_model(self, model: Model):
        try:
            optimizer: optimizers = self.select_optimizer(opt_name=self.optimizer_name)

            self.metrics += [tf.keras.metrics.Precision(),
                             tf.keras.metrics.Recall(),
                             tf.keras.metrics.TruePositives(),
                             tf.keras.metrics.TrueNegatives()]
            model.compile(
                optimizer=optimizer,
                loss=self.losses,
                metrics=self.metrics)
        except Exception as e:
            gv.logger.error(e)
        return model

    def train_model(self, model: Model, mongo_manager: MongoDBManager,
                    collection_name: str, train_ids: list, dev_ids: list):
        history: Optional[History] = None
        try:
            steps_per_epoch = int(len(train_ids) / self.batch_size)
            validation_steps = int(len(dev_ids) / self.batch_size)

            training_generator: DataGenerator = DataGenerator(
                mongo_manager=mongo_manager,
                docs_uuids=train_ids,
                collection_name=collection_name,
                batch_size=self.batch_size,
                dim_embd=self.input_dim,
                dim_output=self.output_dim,
                shuffle=True, to_fit=True)
            validation_generator: DataGenerator = DataGenerator(
                mongo_manager=mongo_manager,
                docs_uuids=dev_ids,
                collection_name=collection_name,
                batch_size=self.batch_size,
                dim_embd=self.input_dim,
                dim_output=self.output_dim,
                shuffle=True, to_fit=True)

            history: History = model.fit_generator(
                training_generator,
                validation_data=validation_generator,
                steps_per_epoch=steps_per_epoch,
                validation_steps=validation_steps,
                class_weight=training_generator.class_weights,
                epochs=self.epochs,
                callbacks=self.callbacks)
        except Exception as e:
            gv.logger.error(e)
        return history

    @staticmethod
    def execute_prediction(model: Model, input_test_doc: np.ndarray):
        y_hat: np.ndarray = np.array([])
        try:
            y_hat: np.ndarray = model.predict(input_test_doc)

        except Exception as e:
            gv.logger.error(e)
        return y_hat

    @staticmethod
    def get_model_predictions(model: Model, x_docs: np.ndarray):
        y_hats: np.ndarray = np.array([])
        try:
            # 1. Execute prediction
            y_hats: np.ndarray = __class__.execute_prediction(
                model=model,
                input_test_doc=x_docs)
            y_hats: np.ndarray = y_hats.ravel().reshape((-1, 1))
        except Exception as e:
            gv.logger.error(e)
        return y_hats

    @staticmethod
    def evaluate_model(model: Model, x: np.ndarray, y: np.ndarray, metrics_names: list):
        metrics_values: list = model.evaluate(x=x, y=y)
        return dict(zip(metrics_names, metrics_values))

    @staticmethod
    def save_history(history: History, history_directory: str, history_name: str):
        try:
            # Check if directory exists
            prepare_directory(dir_path_to_check=history_directory)
            history_path = os.path.join(history_directory, history_name)
            # Write JSON
            write_json_file(data=history.history, filename=history_path)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def load_history(history_directory: str, history_name: str):
        model_history: Optional[History] = None
        try:
            history_path: str = os.path.join(history_directory, history_name)
            # Load model history
            model_history: History = read_json_file(history_path)
        except Exception as e:
            gv.logger.error(e)
        return model_history

    @staticmethod
    def load_trained_model(model_directory: str, model_name: str):
        model: Optional[Model] = None
        try:
            model_path: str = os.path.join(model_directory, model_name)
            # Load model
            model: Model = load_model(model_path)
        except Exception as e:
            gv.logger.error(e)
        return model

    @staticmethod
    def save_trained_model(model: Model, model_directory: str, model_name: str, extension: str = ".h5"):
        try:
            # Check if directory exists
            prepare_directory(dir_path_to_check=model_directory)
            # Check if extension is not included in the name of the model
            if len(model_name.split(".")) <= 1:
                model_name += extension
            # Merge path
            model_path: str = os.path.join(model_directory, model_name)
            # Save model
            model.save(model_path)
        except Exception as e:
            gv.logger.error(e)
