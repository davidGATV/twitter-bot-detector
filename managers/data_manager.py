import pandas as pd
import numpy as np
import pickle
from helper import global_variables as gv
from managers.mongodb_manager import MongoDBManager
from managers.model_manager import ModelManager
from analysis.feature_extraction import FeatureExtraction
from models.twitter_models import InputFeatureBotDocument
from pymongo.cursor import Cursor
from bson.binary import Binary
from keras.models import Model
from keras.callbacks import History
from typing import Optional


class DataManager:
    def __init__(self):
        self.mongo_manager: MongoDBManager = MongoDBManager(
            host=gv.db_host, port=gv.db_port,
            username=gv.db_username, password=gv.db_password,
            db_name=gv.db_name)
        self.collection_name = gv.collection_name
        self.model_manager: ModelManager = Optional[None]

    @staticmethod
    def load_dataset(filename_dir: str):
        df_twitter_bot: pd.DataFrame = pd.DataFrame([])
        try:
            df_twitter_bot: pd.DataFrame = pd.read_csv(filename_dir, index_col=0)
        except Exception as e:
            gv.logger.error(e)
        return df_twitter_bot

    def init_mongo_connection(self):
        try:
            self.mongo_manager.set_up_db()
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def preprocess_dataframe_train(data: pd.DataFrame, popularity_metric: str, boolean_cols: list,
                                   drop_num_cols: list, scaler_filename: str, cat_cols: list,
                                   default_value: str, link_information: list, document_col: str,
                                   target_col: str, join_attr: str = ". "):
        try:
            data: pd.DataFrame = FeatureExtraction.preprocess_dataframe_train(
                data=data, popularity_metric=popularity_metric,
                boolean_cols=boolean_cols,
                drop_num_cols=drop_num_cols,
                scaler_filename=scaler_filename,
                cat_cols=cat_cols,
                default_value=default_value,
                link_information=link_information,
                document_col=document_col,
                target_col=target_col,
                join_attr=join_attr)
        except Exception as e:
            gv.logger.error(e)
        return data

    def generate_features_from_dataframe(self, data: pd.DataFrame, popularity_metric: str,
                                         boolean_cols: list, drop_num_cols: list,
                                         scaler_filename: str, cat_cols: list,
                                         default_value: str, link_information: list,
                                         document_col: str, id_col: str,
                                         target_col: str, join_attr: str = ". ",
                                         update: bool = True):
        try:
            # 1. Preprocess data
            data: pd.DataFrame = self.preprocess_dataframe_train(
                    data=data, popularity_metric=popularity_metric,
                    boolean_cols=boolean_cols,
                    drop_num_cols=drop_num_cols,
                    scaler_filename=scaler_filename,
                    cat_cols=cat_cols,
                    default_value=default_value,
                    link_information=link_information,
                    document_col=document_col,
                    target_col=target_col,
                    join_attr=join_attr)

            # 2. Retrieve relevant information for the analysis
            all_num_cols: list = FeatureExtraction.get_numerical_columns(data=data)
            all_num_cols.remove(id_col)
            embeddings: list = FeatureExtraction.get_flair_embeddings()

            # 3.Start Process
            for index, row in data.iterrows():
                try:
                    gv.logger.warn("Processing Twitter Account %s/%s", int(index) + 1,
                                   str(data.shape[0]))

                    uuid: str = str(row[id_col])
                    account_type: str = row[target_col]

                    # ===================================================
                    filter_data: dict = {"uuid": uuid}
                    not_exist: bool = self.find_document_in_mongodb(
                        filter_data=filter_data)
                    # ===================================================

                    if not_exist or update:
                        # 3.1 Generate doc embedding
                        document: str = row[document_col]
                        doc_emb: np.ndarray = FeatureExtraction.generate_doc_embedding(document=document,
                                                                                       embeddings=embeddings)
                        x_doc_emb: list = list(doc_emb.tolist())
                        x_num: list = [row[j] for j in all_num_cols]

                        if len(x_doc_emb) > 0:
                            # 3.3 Concatenate doc embedding + numerical cols
                            doc_embedding_np: np.ndarray = np.array([x_num + x_doc_emb]).reshape((1, -1))
                            print(doc_embedding_np.shape)

                            # 3.4 Create Binary object from Numpy to be stored
                            doc_embedding: Binary = Binary(pickle.dumps(doc_embedding_np, protocol=2))

                            # 3.5 Generate Input Object
                            input_data_doc: InputFeatureBotDocument = InputFeatureBotDocument(
                                uuid=uuid, account_type=account_type,
                                doc_embedding=doc_embedding)

                            # 3.4 Save document in MongoDB
                            self.ingest_document_into_mongodb(input_doc=input_data_doc, update=update)
                        else:
                            gv.logger.warning("Not well created doc embedding.")
                except Exception as er:
                    continue
        except Exception as e:
            gv.logger.error(e)

    def find_document_in_mongodb(self, filter_data: dict):
        not_exist: bool = False
        try:
            # 1. Check MongoDb Connection
            if self.mongo_manager.db is None:
                self.init_mongo_connection()

            # 2. Find document
            res_doc: Cursor = self.mongo_manager.find_document_by_filter(
                collection_name=self.collection_name, filter=filter_data)
            not_exist: bool = False if len(list(res_doc)) else True
        except Exception as e:
            gv.logger.error(e)
        return not_exist

    def ingest_document_into_mongodb(self, input_doc: InputFeatureBotDocument, update: bool = False):
        try:
            # 1. Check MongoDb Connection
            if self.mongo_manager.db is None:
                self.init_mongo_connection()

            # 2. Verify that the document does not exist
            filter_data: dict = {"uuid": input_doc.uuid}
            not_exist: bool = self.find_document_in_mongodb(filter_data=filter_data)

            # 2.1 The document does not exist
            if not_exist:
                gv.logger.info("Ingesting document at %s", self.collection_name)
                self.mongo_manager.insert_document_to_collection(
                    collection_name=self.collection_name,
                    document=input_doc.dict_from_class())

            # 2.2 The document already exists
            else:
                # a) Overwrite the document
                if update:
                    # Find and update
                    self.mongo_manager.find_and_replace_document(
                        collection_name=self.collection_name,
                        filter=filter_data,
                        updated_doc=input_doc.dict_from_class())
        except Exception as e:
            gv.logger.error(e)

    def retrieve_all_documents_ids(self):
        all_docs_ids: iter = iter([])
        try:
            # 1. Check MongoDb Connection
            if self.mongo_manager.db is None:
                self.init_mongo_connection()

            all_docs_ids: iter = self.mongo_manager.find_all_document_ids(
                collection_name=gv.collection_name)
        except Exception as e:
            gv.logger.error(e)
        return all_docs_ids

    def get_input_dim_from_doc(self):
        input_dim: int = 0
        try:
            # 1. Check MongoDb Connection
            if self.mongo_manager.db is None:
                self.init_mongo_connection()

            sampling_doc: dict = self.mongo_manager.find_one_document(
                collection_name=self.collection_name)
            x_doc: np.ndarray = pickle.loads(sampling_doc['doc_embedding']).reshape((1, -1))
            input_dim: int = x_doc.shape[1]

        except Exception as e:
            gv.logger.error(e)
        return input_dim

    def get_embeddings_from_mongo_docs(self, collection_name: str, docs_ids: list,
                                       dim_embd: int, dim_output: int,
                                       output_mapping: dict, embedding_label: str,
                                       target_label: str):

        # Initialization
        x: np.ndarray = np.empty((len(docs_ids), dim_embd))
        y: np.ndarray = np.empty((len(docs_ids), dim_output))

        try:
            for i, doc_id in enumerate(docs_ids):
                # Store sample
                doc: dict = self.mongo_manager.get_document_by_id(
                    collection_name=collection_name,
                    uuid=doc_id)

                x_np: np.ndarray = pickle.loads(doc[embedding_label])
                x[i, ] = x_np
                y[i, ] = output_mapping.get(doc[target_label], -1)
        except Exception as e:
            gv.logger.error(e)
        return x, y

    def train_bot_net(self, model_directory: str, model_name: str, history_directory: str,
                      history_name: str, output_dim: int, inverse_neurons: int,
                      intermediate_layer: str, output_layer: str,
                      output_mapping: dict, embedding_label: str = "doc_embedding",
                      target_label: str = "account_type"):
        try:
            # Retrieve documents
            all_docs_ids: iter = self.retrieve_all_documents_ids()
            input_dim: int = self.get_input_dim_from_doc()
            intermediate_neurons: int = int(np.ceil(input_dim / inverse_neurons))

            # Initialise model object
            self.model_manager: ModelManager = ModelManager(input_dim=input_dim, output_dim=output_dim)

            # Generate sets
            train_ids, dev_ids, test_ids = self.model_manager.generate_train_dev_test_sets(
                all_docs_ids=list(all_docs_ids))

            # Build model
            bot_net: Model = self.model_manager.build_bot_net(
                input_dim=input_dim,
                output_dim=output_dim,
                intermediate_neurons=intermediate_neurons,
                intermediate_layer=intermediate_layer,
                output_layer=output_layer,
                model_name=model_name)

            # Compile model
            bot_net: Model = self.model_manager.compile_model(model=bot_net)

            # Train model
            bot_net_history: History = self.model_manager.train_model(
                model=bot_net, mongo_manager=self.mongo_manager,
                collection_name=self.collection_name, train_ids=train_ids,
                dev_ids=dev_ids)

            # Test predictions
            x_test, y_true_test = self.get_embeddings_from_mongo_docs(collection_name=self.collection_name,
                                                                      docs_ids=test_ids,
                                                                      dim_embd=input_dim,
                                                                      dim_output=output_dim,
                                                                      embedding_label=embedding_label,
                                                                      target_label=target_label,
                                                                      output_mapping=output_mapping)
            metrics_results: dict = self.model_manager.evaluate_model(
                model=bot_net, x=x_test, y=y_true_test, metrics_names=self.model_manager.metrics)
            gv.logger.info("Metrics results for testing set: %s", metrics_results)

            # Save data
            self.save_model_information(model=bot_net, model_history=bot_net_history,
                                        model_directory=model_directory, model_name=model_name,
                                        history_directory=history_directory, history_name=history_name)
        except Exception as e:
            gv.logger.error(e)

    def save_model_information(self, model: Model, model_history: History, model_directory: str,
                               model_name: str, history_directory: str, history_name: str):
        try:
            # Model
            self.model_manager.save_trained_model(model=model, model_directory=model_directory,
                                                  model_name=model_name)
            # Model history
            self.model_manager.save_history(history=model_history,
                                            history_directory=history_directory,
                                            history_name=history_name)
        except Exception as e:
            gv.logger.error(e)