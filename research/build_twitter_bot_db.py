import pandas as pd
import numpy as np
import glob, os
from api.twitter_api import TwitterConnector
from tweepy import API
from langdetect import detect

DATASET_DIR: str = "D:\\DAVID\\Datasets\\Twitter_bots"
parent_dir: str = "resources"
column_id: str = "id"
col_target: str = "account_type"


def load_datasets_from_dir(dataset_dir: str, extension: str = ".tsv"):
    data: pd.DataFrame = pd.DataFrame([])
    try:
        # Loop over all the files
        for file in glob.glob(dataset_dir + "./*" + extension):
            temp_data = pd.read_csv(file, index_col=0, sep="\t", header=None).reset_index(drop=False)
            data = pd.concat([data, temp_data], axis=0,
                             sort=False)
    except Exception as e:
        print(e)
    return data


def rename_columns_from_df(data: pd.DataFrame, old_cols: list, new_cols):
    try:
        cols: dict = {k: v for k,v in zip(old_cols, new_cols)}
        data.rename(columns=cols, inplace=True)
    except Exception as e:
        print(e)
    return data


def query_target_from_account(data_query: pd.DataFrame, account_id: int, column_id: str, col_target: str):
    target: str = ""
    try:
        res: pd.DataFrame = data_query[data_query[column_id] == account_id]
        if res.shape[0] > 0:
            target = res[col_target].values.tolist()[0]
    except Exception as e:
        print(e)
    return target


# language detection
def extract_language(row):
    lang: str = ""
    try:
        text: str = row["description"]
        lang: str = detect(text=text) if row["description"] is not np.nan else ""
    except Exception as e:
        print(e)
    return lang


df_twitter_accounts: pd.DataFrame = load_datasets_from_dir(dataset_dir=DATASET_DIR)

old_cols: list = list(df_twitter_accounts.columns)
new_cols: list = [column_id, col_target]
df_twitter_accounts: pd.DataFrame = rename_columns_from_df(data=df_twitter_accounts,
                                                           old_cols=old_cols,
                                                           new_cols=new_cols)

tw_connector: TwitterConnector = TwitterConnector()
user_ids: list = list(set(df_twitter_accounts[column_id].values.tolist()))

tw_connector.set_up_connection()
api: API = tw_connector.api

res: list = tw_connector.generate_twitter_account_database(
    user_ids=user_ids,
    api=api)

# Build DataFrame
df_dataset_twitter: pd.DataFrame = pd.DataFrame().from_records(res)
all_user_ids: list = df_dataset_twitter[column_id].values.tolist()
all_available_targets: list = [query_target_from_account(data_query=df_twitter_accounts,
                                                         account_id=i, column_id=column_id,
                                                         col_target=col_target) for i in all_user_ids]
df_dataset_twitter[col_target] = all_available_targets

print(df_dataset_twitter[df_dataset_twitter[col_target] == 'human'].shape[0])
print(df_dataset_twitter[df_dataset_twitter[col_target] == 'bot'].shape[0])

# Add language
df_dataset_twitter["lang"] = df_dataset_twitter.apply(extract_language, axis=1)


# Save data
twitter_db_filename: str = os.path.join(parent_dir,
                                        "twitter_account_db.csv")
df_dataset_twitter.to_csv(twitter_db_filename)

