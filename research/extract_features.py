import pandas as pd
import os
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from flair.embeddings import FlairEmbeddings, DocumentPoolEmbeddings
from sklearn.preprocessing import QuantileTransformer
from flair.data import Sentence
from torch import Tensor
import matplotlib as mpl


mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['figure.figsize'] = 12, 8
mpl.rcParams['font.sans-serif'] = ['Tahoma']
sns.set(font_scale=1.5)
sns.set_style("whitegrid")


def plot_multiple_histograms(data: pd.DataFrame, grouped: str, labels: list, target: str,
                             colors: list, save=False, filename: str = None):
    # Plot
    kwargs = dict(hist_kws={'alpha': .6}, kde_kws={'linewidth': 2})
    plt.figure(figsize=(12, 10))
    title = "\n"
    for j, i in enumerate(labels):
        x = data.loc[data[grouped] == i, target]
        mu_x = round(float(np.mean(x)), 3)
        sigma_x = round(float(np.std(x)), 3)
        ax = sns.distplot(x, color=colors[j], label=i)
        ax.axvline(mu_x, color=colors[j], linestyle='--')
        ax.set(xlabel='Popularity Metric', ylabel='Density')
        title += "Parámetros {}: $G(\mu=$ {}, $\sigma=$ {} \n".format(str(i), str(mu_x), str(sigma_x))
        ax.set_title(title)
    plt.legend()
    plt.grid()
    plt.tight_layout()
    if save:
        plt.savefig(filename + "_histogram.png")
    plt.show()
    plt.grid()


filename_dir = os.path.join("resources", "twitter_bots_human_dataset.csv")
df_twitter_bot: pd.DataFrame = pd.read_csv(filename_dir, index_col=0)


def popularity_metric(friends_count: int, followers_count: int):
    return np.round(np.log(1+friends_count) * np.log(1+followers_count), 3)

# np.round(np.log(1+friends_count) * np.log(1+followers_count), 3)


def compute_popularity_metric(row):
    return popularity_metric(friends_count=row["friends_count"],
                             followers_count=row["followers_count"])


def convert_bool_to_int(data: pd.DataFrame, boolean_cols: list):
    try:
        for col in boolean_cols:
            data[col] = data[col].astype(int)
    except Exception as e:
        print(e)
    return data


palette: str = "husl"
target: str = "popularity"
grouped: str = "account_type"
default_value: str = "unknown"
boolean_cols: list = ["default_profile", "default_profile_image",
                      "geo_enabled", "verified"]
num_cols_drop: list = ["id"]
cat_cols: list = ["description", "screen_name", "lang"]

df_twitter_bot = convert_bool_to_int(data=df_twitter_bot, boolean_cols=boolean_cols)
df_twitter_bot[target] = df_twitter_bot.apply(compute_popularity_metric, axis=1)
labels: list = df_twitter_bot.account_type.unique().tolist()
colors: list = sns.color_palette(palette, len(labels))

print(df_twitter_bot[df_twitter_bot[grouped] == 'human'].shape[0])
print(df_twitter_bot[df_twitter_bot[grouped] == 'bot'].shape[0])


# Extract histograms
plot_multiple_histograms(data=df_twitter_bot, grouped=grouped, labels=labels,
                         target=target, save=False, filename=None,
                         colors=colors)

# ---------------------------------------------------
# Extract numerical and categorical features
# ---------------------------------------------------


# Preprocess and scale data
df_features = df_twitter_bot.copy()
df_features.drop([grouped], axis=1, inplace=True)

df_num = df_features._get_numeric_data()
df_num.drop(num_cols_drop, axis=1, inplace=True)
scaler = QuantileTransformer()
df_num_scaled = scaler.fit_transform(df_num)


# Preprocess numerical information
df_cat = df_features.select_dtypes(["object"])
df_cat = df_cat[cat_cols]
df_cat.fillna(value=default_value, inplace=True)

jw_forward: FlairEmbeddings = FlairEmbeddings("multi-forward")
jw_backward: FlairEmbeddings = FlairEmbeddings("multi-backward")


def generate_doc_embedding(document: str, embeddings: list):
    doc_embedding: np.ndarray = np.array([])
    try:
        # 1. Initialise Document Embedding
        document_embeddings: DocumentPoolEmbeddings = DocumentPoolEmbeddings(
            embeddings=embeddings)
        # 2. Create an example sentence
        sentence: Sentence = Sentence(document)

        # 3. Embed the sentence with our document embedding
        document_embeddings.embed(sentence)

        # 4. Save embedding into CPU
        doc_emb_cpu: Tensor = sentence.embedding.cpu()

        # 5. Convert to numpy array
        doc_embedding: np.ndarray = doc_emb_cpu.detach().numpy()
    except Exception as e:
        print(e)
    return doc_embedding


def generate_document_from_features(text_features: list, link_information: list,
                                    join_attr: str = ". "):
    document: str = ""
    try:
        document_lst: list = [link + " : " + text for text, link in zip(text_features,
                                                                        link_information)]
        document: str = join_attr.join(document_lst)
    except Exception as e:
        print(e)
    return document


# Generate documents for twitter accounts
def extract_document_from_categorical_data(row: pd.Series, link_information: list, join_attr: str = ". "):
    document: str = ""
    try:
        text_features: list = [row[col] for col in list(row.index)]
        document: str = generate_document_from_features(text_features=text_features,
                                                        link_information=link_information,
                                                        join_attr=join_attr)
    except Exception as e:
        print(e)
    return document


link_information: list = ["The description is", "The screen name is", "The language is"]
join_attr: str = ". "
df_cat["document"] = df_cat.apply(extract_document_from_categorical_data, axis=1,
                                  args=(link_information, join_attr,))

embeddings: list = [jw_forward, jw_backward]
all_documents: list = df_cat["document"].values.tolist()
all_doc_embeddings: list = [generate_doc_embedding(i,
                                                   embeddings=embeddings) for i in all_documents]
