import tweepy as tw
import numpy as np
from tweepy import API
from tweepy.models import User, ResultSet
from helper import global_variables as gv
from models.twitter_models import UserProfile
from helper.utils import int_to_str_list, chunks_from_list
from typing import Optional


class TwitterConnector:
    def __init__(self):
        self.consumer_key = gv.CONSUMER_KEY
        self.consumer_secret = gv.CONSUMER_SECRET
        self.access_token = gv.ACCESS_TOKEN
        self.access_token_secret = gv.ACCESS_TOKEN_SECRET
        self.api: Optional[API] = None
        if gv.logger is None:
            gv.init_logger_object()

    def set_up_connection(self):
        try:
            gv.logger.info("Connecting to Twitter API ... ")
            auth = tw.OAuthHandler(self.consumer_key, self.consumer_secret)
            auth.set_access_token(self.access_token, self.access_token_secret)
            self.api = API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True,
                           retry_count=10, retry_delay=5, retry_errors=5)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def tweet_gathering(api: {API}, search_words: {str}, date_since: {str},
                        items: {int} = 1000, lang: {str} = 'en'):
        tweets = None
        try:
            gv.logger.info("Retrieving Tweets ... ")
            # Collect tweets
            tweets = tw.Cursor(api.search,
                               lang=lang,
                               q=(search_words),
                               since=date_since,
                               include_entities=True,
                               monitor_rate_limit=True,
                               wait_on_rate_limit=True,
                               tweet_mode='extended').items(items)
        except Exception as e:
            gv.logger.error(e)
        return tweets

    @staticmethod
    def get_user_profiles_by_ids(api: API, user_ids: list):
        max_iter: int = 100
        users_data: list = []
        try:
            if len(user_ids) > max_iter:
                n: int = int(np.ceil(len(user_ids)/max_iter))
                data_chunks: list = chunks_from_list(data_ls=user_ids, n=n)
                gv.logger.info("Retrieve user profiles from Twitter API ... ")
                for i, data in enumerate(data_chunks):
                    partial_user_ids: list = data
                    response: ResultSet = api.lookup_users(user_ids=partial_user_ids)
                    users_data.append(response)
        except Exception as e:
            gv.logger.error(e)
        return users_data

    @staticmethod
    def get_user_profiles_by_screen_names(api: API, screen_names: list):
        max_iter: int = 100
        users_data: list = []
        try:
            gv.logger.info("Retrieve user profiles from Twitter API ... ")
            if len(screen_names) > max_iter:
                n: int = int(np.ceil(len(screen_names)/max_iter))
                data_chunks: list = chunks_from_list(data_ls=screen_names, n=n)
                for i, data in enumerate(data_chunks):
                    response: ResultSet = api.lookup_users(screen_names=data)
                    users_data.append(response)
            else:
                response: ResultSet = api.lookup_users(screen_names=screen_names)
                users_data.append(response)
        except Exception as e:
            gv.logger.error(e)
        return users_data

    @staticmethod
    def retrieve_user_data(user: User):
        return UserProfile(user).dict_from_class()

    @staticmethod
    def preprocess_user_profile_list(user_ids: list):
        return int_to_str_list(data_ls=user_ids)

    @staticmethod
    def generate_twitter_account_database(user_ids: list, api: API):
        user_info: list = []
        try:
            user_ids: list = __class__.preprocess_user_profile_list(user_ids)
            response: list = __class__.get_user_profiles_by_ids(api=api,
                                                                user_ids=user_ids)
            gv.logger.info("Extract user profile information")
            user_info: list = [__class__.retrieve_user_data(user) for result in response for user in result]
        except Exception as e:
            gv.logger.error(e)
        return user_info