import os, sys
if "test" in os.getcwd():
    sys.path.append('../')
    parent_dir = ".."
else:
    parent_dir = ""

from analysis.bot_detection_analysis import BotDetectorAnalysis
from tweepy.models import ResultSet, User
import pandas as pd
from helper import global_variables as gv

if gv.logger is None:
    gv.init_logger_object()

botAnalysis: BotDetectorAnalysis = BotDetectorAnalysis()

screen_name: str = "geeksforgeeks"
popularity_metric: str = "popularity"
id_col: str = "id"
boolean_cols: list = ["default_profile", "default_profile_image",
                      "geo_enabled", "verified"]
drop_num_cols: list = [id_col]

cat_cols: list = ["description", "screen_name", "lang"]
scaler_filename: str = os.path.join(parent_dir, "scaler.pkl")
default_value: str = "unknown"
link_information: list = ["The description is",
                          "The screen name is",
                          "The language is"]
document_col: str = "document"
target_col: str = "account_type"

# 1. Retrieve user account by screen name
users_info: list = botAnalysis.get_user_data_by_screen_name(screen_names=[screen_name])
users_info: ResultSet = users_info[0]
user_data: User = users_info[0]

# 2. Extract features
user_features: dict = botAnalysis.extract_features_from_user_account(
    user_data=user_data)

# 3. Transform data to extract embedings
data_transformed: pd.DataFrame = botAnalysis.preprocess_data(
    user_features=user_features,
    popularity_metric=popularity_metric,
    boolean_cols=boolean_cols,
    drop_num_cols=drop_num_cols,
    scaler_filename=scaler_filename,
    cat_cols=cat_cols,
    default_value=default_value,
    link_information=link_information,
    document_col=document_col,
    join_attr=". ")

# 4. Generate input embedding

# 5. Load model

# 6. Predict results