import os, sys
if "test" in os.getcwd():
    sys.path.append('../')
    parent_dir = ".."
else:
    parent_dir = ""

from managers.data_manager import DataManager
from helper import global_variables as gv

if gv.logger is None:
    os.environ['APP_LOGS'] = parent_dir
    gv.init_logger_object()

dm: DataManager = DataManager()

# ========================================================
# ============= DO NOT CHANGE ============================
# ========================================================
popularity_metric: str = "popularity"
id_col: str = "id"
boolean_cols: list = ["default_profile", "default_profile_image",
                      "geo_enabled", "verified"]
drop_num_cols: list = [id_col]

cat_cols: list = ["description", "screen_name", "lang"]
result_params: str = "result_params"
scaler_filename: str = os.path.join(parent_dir, result_params, "scaler.pkl")
default_value: str = "unknown"
link_information: list = ["The description is",
                          "The screen name is",
                          "The language is"]
document_col: str = "document"
target_col: str = "account_type"
model_directory: str = os.path.join(parent_dir, result_params, "trained_models")
# ========================================================

# =======================================================
# Can be changed ========================================
model_name: str = gv.model_name
history_directory: str = model_directory
history_name: str = model_name + "_history.json"
output_dim: int = 1
intermediate_layer: str = "relu"
output_layer: str = "sigmoid"
output_mapping: dict = {"human": 0, "bot": 1}
inverse_neurons: int = 3
# =======================================================


res_tr = dm.train_bot_net(
        model_directory=model_directory,
        model_name=model_name,
        history_directory=history_directory,
        history_name=history_name,
        output_dim=output_dim,
        intermediate_layer=intermediate_layer,
        output_layer=output_layer,
        inverse_neurons=inverse_neurons,
        output_mapping=output_mapping)