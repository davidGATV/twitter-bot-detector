import pandas as pd
import os, sys

if "test" in os.getcwd():
    sys.path.append('../')
    parent_dir = ".."
else:
    parent_dir = ""

from managers.data_manager import DataManager
from helper import global_variables as gv

if gv.logger is None:
    os.environ['APP_LOGS'] = parent_dir
    gv.init_logger_object()

dm: DataManager = DataManager()

resources_dir = os.path.join(parent_dir, "resources")
dataset_dir = "twitter_bots_human_dataset.csv"
dataset_dir = os.path.join(resources_dir, dataset_dir)

data: pd.DataFrame = dm.load_dataset(filename_dir=dataset_dir)

# ========================================================
# ============= DO NOT CHANGE ============================
# ========================================================
popularity_metric: str = "popularity"
id_col: str = "id"
boolean_cols: list = ["default_profile", "default_profile_image",
                      "geo_enabled", "verified"]
drop_num_cols: list = [id_col]

cat_cols: list = ["description", "screen_name", "lang"]
result_params: str = "result_params"
scaler_filename: str = os.path.join(parent_dir, result_params, "scaler.pkl")
default_value: str = "unknown"
link_information: list = ["The description is",
                          "The screen name is",
                          "The language is"]
document_col: str = "document"
target_col: str = "account_type"
# ========================================================

# =======================================================
# Can be changed ========================================
model_directory: str = os.path.join(parent_dir, result_params, "trained_models")
model_name: str = gv.model_name
history_directory: str = model_directory
history_name: str = model_name + "_history.json"
output_dim: int = 1
intermediate_layer: str = "relu"
output_layer: str = "sigmoid"
output_mapping: dict = {"human": 0, "bot": 1}
inverse_neurons: int = 3
# =======================================================

res = dm.generate_features_from_dataframe(
    data=data,
    popularity_metric=popularity_metric,
    boolean_cols=boolean_cols,
    drop_num_cols=drop_num_cols,
    scaler_filename=scaler_filename,
    cat_cols=cat_cols,
    default_value=default_value,
    link_information=link_information,
    document_col=document_col,
    id_col=id_col,
    target_col=target_col,
    join_attr=". ",
    update=False)
