from tweepy.models import User
from dotmap import DotMap
from datetime import datetime
from helper.utils import preprocess_date, get_average_tweets_per_day, get_account_age_in_days
from bson.binary import Binary


class TwitterDocument:
    def dict_from_class(self):
        return dict((key, value)
                    for (key, value) in self.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)


class UserProfile(TwitterDocument):
    def __init__(self, user_profile: User):
        self.created_at: datetime = preprocess_date(user_profile.created_at)
        self.default_profile: bool = user_profile.default_profile
        self.default_profile_image: bool = user_profile.default_profile_image
        self.description: str = user_profile.description
        self.favourites_count: int = user_profile.favourites_count
        self.followers_count: int = user_profile.followers_count
        self.friends_count: int = user_profile.friends_count
        self.geo_enabled: bool = user_profile.geo_enabled
        self.id: int = user_profile.id
        self.lang: str = "" if user_profile.lang is None else user_profile.lang
        self.location: str = "" if user_profile.location is None else user_profile.location
        self.profile_background_image_url: str = user_profile.profile_background_image_url
        self.profile_image_url: str = user_profile.profile_image_url
        self.screen_name: str = user_profile.screen_name
        self.statuses_count: int = user_profile.statuses_count
        self.verified: bool = user_profile.verified
        self.average_tweets_per_day: float = get_average_tweets_per_day(
            statuses_count=self.statuses_count, created_at=user_profile.created_at)
        self.account_age_days: int = get_account_age_in_days(created_at=user_profile.created_at)


class InputFeatureBotDocument(TwitterDocument):
    def __init__(self, uuid: str,
                 account_type: str, doc_embedding: Binary):
        self.uuid: str = uuid
        self.account_type: str = account_type
        self.doc_embedding: Binary = doc_embedding