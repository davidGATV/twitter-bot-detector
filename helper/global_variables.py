import os
import warnings
from helper.custom_log import init_logger
warnings.filterwarnings('ignore')


# ================================================================
# MONGODB Params
db_host: str = os.getenv("MONGODB_HOST") if "MONGODB_HOST" in os.environ else "localhost"
db_port: str = os.getenv("MONGODB_PORT") if "MONGODB_PORT" in os.environ else "27017"
db_username: str = os.getenv("MONGODB_USERNAME") if "MONGODB_USERNAME" in os.environ else ""
db_password: str = os.getenv("MONGODB_PASSWORD") if "MONGODB_PASSWORD" in os.environ else ""
db_name: str = os.getenv("MONGODB_NAME") if "MONGODB_NAME" in os.environ else "twitter_human_bots"
collection_name: str = os.getenv("MONGODB_COLLECTION_NAME") if "MONGODB_COLLECTION_NAME" in\
                                                               os.environ else "doc_features"

# ================================================================
# Twitter Data
CONSUMER_KEY = "WcIJMyBF3spFEtRGHwqDlAKDT"
CONSUMER_SECRET = "IfNlsgNrW5o6rfhayOURF1BcUDBcsrfgqss9g5inqeFytDbkgE"
ACCESS_TOKEN = "973174862159253505-mAYpqjzegRXFNhdEPXOkDLsHWXePp7q"
ACCESS_TOKEN_SECRET = "0mmfQcNDJBIFhMM9bexZSO1eIKhFdaP8JX9cMnBG81gJE"

# =========================================================
logger = None
link_information: list = ["The description is", "The screen name is", "The language is"]
join_attr: str = ". "
# =========================================================

test_size: float = 0.2
dev_size: float = 0.3
model_name: str = os.getenv("BOT_NET_NAME") if "BOT_NET_NAME" in os.environ else "BOT-NET"
metrics: list = ["accuracy"]
loss: str = "binary_crossentropy"
optimizer_name: str = "adam"
batch_size: int = 256
epochs: int = 300
monitor_early_stopping: str = "val_loss"
patience: int = int(epochs/5)


def init_logger_object():
    global logger
    logger = init_logger(__name__, testing_mode=False)