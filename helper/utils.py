import numpy as np
import os, json
from datetime import datetime
from helper import global_variables as gv
import more_itertools as mit


def preprocess_date(non_formatted_date: datetime,
                    new_format: str = '%Y-%m-%d %H:%M:%S'):
    return non_formatted_date.strftime(new_format)


def get_average_tweets_per_day(statuses_count: int, created_at: datetime):
    average_tweets_per_day: float = 0.0
    try:
        account_age_days = get_account_age_in_days(created_at=created_at)
        average_tweets_per_day: float = float(np.round(statuses_count / account_age_days, 3))
    except Exception as e:
        gv.logger.error(e)
    return average_tweets_per_day


def get_account_age_in_days(created_at: datetime):
    delta = datetime.utcnow() - created_at
    return delta.days


def int_to_str_list(data_ls: list):
    return [str(element) for element in data_ls]


def chunks_from_list(data_ls: list, n: int):
    """Yield successive n-sized chunks from lst."""
    split_lst: list = [list(el) for el in mit.divide(n, data_ls)]
    return split_lst


def prepare_directory(dir_path_to_check: str):
    res: bool = False
    try:
        if not os.path.exists(dir_path_to_check):
            os.makedirs(dir_path_to_check)
        res = True
    except Exception as e:
        gv.logger.error(e)
    return res


def write_json_file(data: dict, filename: str):
    try:
        json_data = json.dumps(str(data))
        with open(filename, 'w') as file:
            json.dump(json_data, file)
    except Exception as e:
        gv.logger.error(e)


def read_json_file(filename):
    data = None
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
    except Exception as e:
        gv.logger.error(e)
    return data
