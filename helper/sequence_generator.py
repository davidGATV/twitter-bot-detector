import numpy as np
import pickle
from helper import global_variables as gv
from tensorflow import keras
from managers.mongodb_manager import MongoDBManager
from helper.nd_standard_scaler import NDStandardScaler
from sklearn.utils import class_weight


class DataGenerator(keras.utils.Sequence):
    """Generates data for Keras
    Sequence based data generator. Suitable for building data generator for training and prediction.
    """
    def __init__(self, mongo_manager: MongoDBManager, docs_uuids: list, collection_name: str,
                 batch_size: int, dim_embd: int, dim_output: int,
                 shuffle: bool = True, to_fit: bool = True):

        self.mongo_manager: MongoDBManager = mongo_manager
        self.docs_uuids: list = docs_uuids
        self.collection_name: str = collection_name
        self.batch_size: int = batch_size
        self.dim_embd: int = dim_embd
        self.dim_output: int = dim_output
        self.shuffle = shuffle
        self.to_fit: bool = to_fit
        self.indexes: np.arange = np.arange(len(self.docs_uuids))
        self.scaler: NDStandardScaler = NDStandardScaler()
        self.class_weights: list = []
        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return int(np.floor(len(self.docs_uuids) / self.batch_size))

    def __getitem__(self, index):
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        docs_ids_temp: list = [self.docs_uuids[k] for k in indexes]

        # Generate data
        x, y = self.generate_data(docs_ids_temp, to_fit=self.to_fit)
        if self.to_fit:
            self.class_weights: list = self.get_class_weights(y_train=y)
            return x, y
        else:
            return x

    def on_epoch_end(self):
        """Updates indexes after each epoch
        """
        self.indexes: np.arange = np.arange(len(self.docs_uuids))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def generate_data(self, doc_ids: list, to_fit: bool = True):
        # Initialization
        x: np.ndarray = np.empty((self.batch_size, self.dim_embd))
        y: np.ndarray = np.empty((self.batch_size, self.dim_output))

        # Generate data
        for i, doc_id in enumerate(doc_ids):
            # Store sample
            doc: dict = self.load_data_from_mongodb(
                collection_name=self.collection_name,
                doc_id=doc_id)
            x_np: np.ndarray = pickle.loads(doc['doc_embedding'])
            x[i, ] = x_np
            if to_fit:
                output_mapping: dict = self.mapping_target()
                y[i, ] = output_mapping.get(doc["account_type"], -1)
        return x, y

    def load_data_from_mongodb(self, collection_name: str, doc_id: str):
        doc_data: dict = {}
        try:
            doc_data: dict = self.mongo_manager.get_document_by_id(collection_name=collection_name,
                                                                   uuid=doc_id)

        except Exception as e:
            gv.logger.error(e)
        return doc_data

    @staticmethod
    def mapping_target():
        output_mapping: dict = {"human": 0, "bot": 1}
        return output_mapping

    @staticmethod
    def get_class_weights(y_train: np.ndarray):
        class_weights: list = []
        try:
            class_weights_np: np.ndarray = class_weight.compute_class_weight(
                'balanced',
                np.unique(y_train.ravel()),
                y_train.ravel())
            class_weights: list = list(class_weights_np.tolist())
        except Exception as e:
            gv.logger.error(e)
        return class_weights