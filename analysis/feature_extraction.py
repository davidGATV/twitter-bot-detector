import pandas as pd
import numpy as np
import joblib, os
from flair.data import Sentence
from torch import Tensor
from helper.utils import prepare_directory
from helper import global_variables as gv
from sklearn.preprocessing import QuantileTransformer
from typing import Optional
from flair.embeddings import FlairEmbeddings, DocumentPoolEmbeddings


class FeatureExtraction:
    @staticmethod
    def generate_doc_embedding(document: str, embeddings: list):
        doc_embedding: np.ndarray = np.array([])
        try:
            gv.logger.info("Generating embedding for document .... ")
            # 1. Initialise Document Embedding
            document_embeddings: DocumentPoolEmbeddings = DocumentPoolEmbeddings(
                embeddings=embeddings)
            # 2. Create an example sentence
            sentence: Sentence = Sentence(document)

            # 3. Embed the sentence with our document embedding
            document_embeddings.embed(sentence)

            # 4. Save embedding into CPU
            doc_emb_cpu: Tensor = sentence.embedding.cpu()

            # 5. Convert to numpy array
            doc_embedding: np.ndarray = doc_emb_cpu.detach().numpy()
        except Exception as e:
            gv.logger.error(e)
        return doc_embedding

    @staticmethod
    def generate_document_from_features(text_features: list, link_information: list,
                                        join_attr: str = ". "):
        document: str = ""
        try:
            document_lst: list = [link + " : " + text for text, link in zip(text_features,
                                                                            link_information)]
            document: str = join_attr.join(document_lst)
        except Exception as e:
            gv.logger.error(e)
        return document

    @staticmethod
    def extract_document_from_categorical_series(row: pd.Series, link_information: list,
                                                 join_attr: str = ". "):
        document: str = ""
        try:
            text_features: list = [row[col] for col in list(row.index)]
            document: str = __class__.generate_document_from_features(
                text_features=text_features,
                link_information=link_information,
                join_attr=join_attr)
        except Exception as e:
            print(e)
        return document

    @staticmethod
    def convert_bool_to_int(data: pd.DataFrame, boolean_cols: list):
        try:
            for col in boolean_cols:
                data[col] = data[col].astype(int)
        except Exception as e:
            gv.logger.error(e)
        return data

    @staticmethod
    def popularity_metric(friends_count: int, followers_count: int):
        return np.round(np.log(1 + friends_count) * np.log(1 + followers_count), 3)

    @staticmethod
    def compute_popularity_metric(row, col_friends_count: str = "friends_count",
                                  col_followers_count: str = "followers_count"):
        return __class__.popularity_metric(
            friends_count=row[col_friends_count],
            followers_count=row[col_followers_count])

    @staticmethod
    def preprocess_dataframe_train(data: pd.DataFrame, popularity_metric: str, boolean_cols: list,
                                   drop_num_cols: list, scaler_filename: str, cat_cols: list,
                                   default_value: str, link_information: list, document_col: str,
                                   target_col: str, join_attr: str = ". "):

        data_transformed: pd.DataFrame = pd.DataFrame([])
        try:
            # Save target variable
            data_target: pd.DataFrame = data[[target_col]]

            # Convert to int boolean
            data: pd.DataFrame = __class__.convert_bool_to_int(
                data=data, boolean_cols=boolean_cols)

            # Extract popularity metric
            data[popularity_metric] = data.apply(FeatureExtraction.compute_popularity_metric,
                                                 axis=1)

            df_features: pd.DataFrame = data.copy()

            gv.logger.info("Preprocessing Numerical Columns")
            # Preprocess Numerical Features
            data_transformed_num: pd.DataFrame = __class__.preprocess_numerical_data(
                data=df_features,
                drop_cols=drop_num_cols,
                scaler_filename=scaler_filename)

            gv.logger.info("Preprocessing Categorical Columns")
            # Preprocess Categorical Features
            data_transformed_cat: pd.DataFrame = __class__.preprocess_categorical_data(
                data=df_features,
                cat_cols=cat_cols,
                default_value=default_value,
                link_information=link_information,
                document_col=document_col,
                join_attr=join_attr)

            gv.logger.info("Concatenating Transformed Columns")
            # Concatenate DataFrames
            data_transformed: pd.DataFrame = pd.concat([data_transformed_num,
                                                        data_transformed_cat,
                                                        data_target],
                                                       axis=1, sort=False)

        except Exception as e:
            gv.logger.error(e)
        return data_transformed

    @staticmethod
    def get_numerical_columns(data: pd.DataFrame):
        return list(data._get_numeric_data().columns)

    @staticmethod
    def preprocess_numerical_data(data: pd.DataFrame, drop_cols: list, scaler_filename: str,
                                  fit=True):
        data_transformed: pd.DataFrame = pd.DataFrame([])
        try:
            # Extract Numerical Features
            df_num: pd.DataFrame = data._get_numeric_data()
            df_num.drop(drop_cols, axis=1, inplace=True)

            # Scale data
            data_transformed: pd.DataFrame = __class__.scale_data(
                data=df_num, fit=fit, filename=scaler_filename)

            # Add drop cols
            data_transformed: pd.DataFrame = pd.concat([data[drop_cols],
                                                        data_transformed], axis=1, sort=False)

        except Exception as e:
            gv.logger.error(e)
        return data_transformed

    @staticmethod
    def preprocess_categorical_data(data: pd.DataFrame, cat_cols: list, default_value: str,
                                    link_information: list, document_col: str,
                                    join_attr: str = ". "):
        data_transformed: pd.DataFrame = pd.DataFrame([])
        try:
            df_cat: pd.DataFrame = data.select_dtypes(["object"])
            df_cat: pd.DataFrame = df_cat[cat_cols]
            df_cat.fillna(value=default_value, inplace=True)

            # Add document
            df_cat[document_col] = df_cat.apply(__class__.extract_document_from_categorical_series,
                                                axis=1,
                                                args=(link_information, join_attr,))

            data_transformed: pd.DataFrame = df_cat.copy()

        except Exception as e:
            gv.logger.error(e)
        return data_transformed

    @staticmethod
    def scale_data(data: pd.DataFrame, fit: bool = True, filename: str = ""):
        data_transformed: pd.DataFrame = pd.DataFrame([])
        try:
            if fit:
                scaler: QuantileTransformer = QuantileTransformer()
                scaler.fit(data)

                prepare_directory(os.sep.join(filename.split(os.sep)[0:-1]))
                # Save scaler
                gv.logger.info("Saving scaler object at %s", filename)

                __class__.save_scaler_object(scaler_obj=scaler, filename=filename)
            else:

                scaler: QuantileTransformer = __class__.load_scaler_object(
                    filename=filename)

            # Transform data
            res_transformed: np.ndarray = scaler.transform(data)
            data_transformed: pd.DataFrame = pd.DataFrame(res_transformed,
                                                          columns=data.columns)
        except Exception as e:
            gv.logger.error(e)
        return data_transformed

    @staticmethod
    def get_flair_embeddings():
        jw_forward: FlairEmbeddings = FlairEmbeddings("multi-forward", chars_per_chunk=128)
        jw_backward: FlairEmbeddings = FlairEmbeddings("multi-backward", chars_per_chunk=128)
        embeddings: list = [jw_forward, jw_backward]
        return embeddings

    @staticmethod
    def save_scaler_object(scaler_obj: QuantileTransformer,
                           filename: str):
        try:
            joblib.dump(scaler_obj, filename)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def load_scaler_object(filename: str):
        scaler_obj: Optional[QuantileTransformer] = None
        try:
            scaler_obj: QuantileTransformer = joblib.load(filename)
        except Exception as e:
            gv.logger.error(e)
        return scaler_obj

    @staticmethod
    def preprocess_prediction_data(data_dct: dict, popularity_metric: str, boolean_cols: list,
                                   drop_num_cols: list, scaler_filename: str, cat_cols: list,
                                   default_value: str, link_information: list, document_col: str,
                                   join_attr: str = ". "):

        data_transformed: pd.DataFrame = pd.DataFrame([])
        try:
            data: pd.DataFrame = pd.DataFrame(data_dct, index=[0])

            # Convert to int boolean
            data: pd.DataFrame = __class__.convert_bool_to_int(
                data=data, boolean_cols=boolean_cols)

            # Extract popularity metric
            data[popularity_metric] = data.apply(FeatureExtraction.compute_popularity_metric,
                                                 axis=1)

            df_features: pd.DataFrame = data.copy()

            gv.logger.info("Preprocessing Numerical Columns")
            # Preprocess Numerical Features
            data_transformed_num: pd.DataFrame = __class__.preprocess_numerical_data(
                data=df_features,
                drop_cols=drop_num_cols,
                scaler_filename=scaler_filename, fit=False)

            gv.logger.info("Preprocessing Categorical Columns")
            # Preprocess Categorical Features
            data_transformed_cat: pd.DataFrame = __class__.preprocess_categorical_data(
                data=df_features,
                cat_cols=cat_cols,
                default_value=default_value,
                link_information=link_information,
                document_col=document_col,
                join_attr=join_attr)

            gv.logger.info("Concatenating Transformed Columns")
            # Concatenate DataFrames
            data_transformed: pd.DataFrame = pd.concat([data_transformed_num,
                                                        data_transformed_cat],
                                                       axis=1, sort=False)
        except Exception as e:
            gv.logger.error(e)
        return data_transformed