import pandas as pd
import numpy as np
from tweepy import API
from tweepy.models import User
from api.twitter_api import TwitterConnector
from helper import global_variables as gv
from analysis.feature_extraction import FeatureExtraction
from typing import Optional


class BotDetectorAnalysis:
    def __init__(self):
        self.twitter_manager: TwitterConnector = TwitterConnector()
        self.embeddings: list = FeatureExtraction.get_flair_embeddings()
        self.api: Optional[API] = None

    def set_up_twitter_api(self):
        if self.twitter_manager.api is None:
            self.twitter_manager.set_up_connection()
        self.api: API = self.twitter_manager.api

    def get_user_data_by_screen_name(self, screen_names: list):
        user_data: list = []
        try:
            # Connect API
            self.set_up_twitter_api()

            # Retrieve data
            user_data: list = self.twitter_manager.get_user_profiles_by_screen_names(
                api=self.api, screen_names=screen_names)
        except Exception as e:
            gv.logger.error(e)
        return user_data

    def extract_features_from_user_account(self, user_data: User):
        user_features: dict = {}
        try:
            # Retrieve user data
            user_features: dict = self.twitter_manager.retrieve_user_data(
                user_data)
        except Exception as e:
            gv.logger.error(e)
        return user_features

    @staticmethod
    def preprocess_data(user_features: dict, popularity_metric: str, boolean_cols: list,
                        drop_num_cols: list, scaler_filename: str, cat_cols: list,
                        default_value: str, link_information: list, document_col: str,
                        join_attr: str = ". "):

        data_transformed: pd.DataFrame = pd.DataFrame([])
        try:
            data_transformed: pd.DataFrame = FeatureExtraction.preprocess_prediction_data(
                data_dct=user_features, popularity_metric=popularity_metric,
                boolean_cols=boolean_cols,
                drop_num_cols=drop_num_cols,
                scaler_filename=scaler_filename,
                cat_cols=cat_cols,
                default_value=default_value,
                link_information=link_information,
                document_col=document_col,
                join_attr=join_attr)
        except Exception as e:
            gv.logger.error(e)
        return data_transformed

    @staticmethod
    def generate_embedding(data: pd.DataFrame, document_col: str, id_col: str, embeddings: list):
        doc_embedding_np: np.ndarray = np.array([])
        try:
            all_num_cols: list = FeatureExtraction.get_numerical_columns(data=data)
            all_num_cols.remove(id_col)

            document: str = data[document_col]

            # 3.1 Generate doc embedding
            doc_emb: np.ndarray = FeatureExtraction.generate_doc_embedding(document=document,
                                                                           embeddings=embeddings)
            x_doc_emb: list = list(doc_emb.tolist())
            x_num: list = [data[j] for j in all_num_cols]

            if len(x_doc_emb) > 0:
                # 3.3 Concatenate doc embedding + numerical cols
                doc_embedding_np: np.ndarray = np.array([x_num + x_doc_emb]).reshape((1, -1))
                print(doc_embedding_np.shape)
        except Exception as e:
            gv.logger.error(e)
        return doc_embedding_np